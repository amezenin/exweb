package dao;

import exweb.Post;
import util.DataSourceProvider;

import java.sql.*;
import java.util.List;

public class PostDao {

    public List<Post> getPosts() {
        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("...")) {
                while (r.next()) {
                    // ...
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

}
